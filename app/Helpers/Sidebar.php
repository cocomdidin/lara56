<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Request;
use Illuminate\Support\Facades\DB;


class Sidebar
{
    public static function menu()
    {
        // dd( Auth::user()->roles->pluck('id') );
        $role_id = Auth::user()->roles->pluck('id');
        $menu = DB::table('menus')
                ->select('id as menu_item_id','parent_id as menu_parent_id','title as menu_item_name','url','menu_order','icon','icon_mini')
                ->wherein('role_id',$role_id)
                ->orderBy('menu_order')
                ->get();
        $refs = array();
        $list = array();
        $hasil ="";
        foreach ($menu as $data) {
            $thisref = &$refs[$data->menu_item_id];
            $thisref['menu_parent_id'] = $data->menu_parent_id;
            $thisref['menu_item_name'] = $data->menu_item_name;
            $thisref['url'] = $data->url;
            $thisref['icon'] = $data->icon;
            $thisref['icon_mini'] = $data->icon_mini;

            if ($data->menu_parent_id == 0)
            {
                $list[$data->menu_item_id] = &$thisref;
            } else
            {
                $refs[$data->menu_parent_id]['children'][$data->menu_item_id] = &$thisref;
            }
        }
        $hasil .=static::create_list( $list,0 );
        return $hasil;
    }

    private static function create_list( $arr ,$urutan)
    {
        $html="";
        foreach ($arr as $key=>$v)
        {
            if (array_key_exists('children', $v))
            {
                $html .='<li class="nav-item '.static::check($v['children'],'active').'">';
                $html .='<a class="nav-link '.static::check($v['children'],'collapsed').'" data-toggle="collapse" href="#'.$v['menu_item_name'].'" aria-expanded="'.static::check($v['children'],'expand').'">
                            <i class="material-icons">'.$v['icon'].'</i>
                            <p>'.$v['menu_item_name'].'
                            <b class="caret"></b>
                            </p>
                        </a>

                        <div class="collapse '.static::check($v['children'],'show').'" id="'.$v['menu_item_name'].'">
                            <ul class="nav">';
                $html .= static::create_list($v['children'],1);
                $html .='</ul> </div>';
            }else
            {
                    $html .='<li class="nav-item '.static::isActive($v['url']).'">';
                    $html .='<a class="nav-link" href="'.$v['url'].'">
                        <i class="material-icons">'.$v['icon'].'</i>';
                        if($urutan==0)
                        {
                            $html .='<p>'.$v['menu_item_name'].'</p>';
                        }else{
                            $html .='<span class="sidebar-mini"> '.$v['icon_mini'].' </span>
                            <span class="sidebar-normal text-truncate" style="max-width: 100%;"> '.$v['menu_item_name'].'</span>
                            </a>';
                        }
            }
            $html .= '</li>';
        }
        return $html;
    }

    public static function isActive($path)
    {
        $alamat=$path;
        if($alamat != '#' and $alamat != '/' )
        {
            $alamat=substr($alamat.'*',1);
        }
        return Request::is($alamat) ? 'active':'';
    }

    public static function check($childs,$is){
        $hasil='';
        foreach ($childs as $key => $child) {
            if (array_key_exists('children', $child)){
                static::check($child['children'],$is);
            }else{
                if(Request::is(substr($child['url'].'*',1))){
                    if($is=='active'){$hasil = 'active';}
                    elseif($is=='expand'){$hasil = 'true';}
                    elseif($is=='show'){$hasil = 'show';}
                    elseif($is=='collapsed'){$hasil = '';}
                    break;
                }else{
                    if($is=='active'){$hasil = '';}
                    elseif($is=='expand'){$hasil = 'false';}
                    elseif($is=='show'){$hasil = '';}
                    elseif($is=='collapsed'){$hasil = 'collapsed';}
                }
            }
        }
        return $hasil;
    }
}
