<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class LaporanController extends Controller
{
    // function __construct()
    // {
    // }
    public function index()
    {
        $poli = DB::connection('simrs')->table("master_polikd")->get();
        $ruang = DB::connection('simrs')->table("master_ruangkd")->get();
        return view('laporan.index',compact('poli','ruang'));
    }
    public function penyakitterbesar($from,$to)
    {
        $icdInap = DB::connection('simrs')->table(DB::raw("(SELECT
                    (CASE 
                        WHEN pok.kelompok='MIL AD' AND sat.satpur IS NOT NULL THEN sat.satpur 
                        WHEN pok.kelompok='MIL AD' AND sat.satpur IS NULL THEN 'SAT BANMIN' 
                        ELSE pok.kelompok 
                    END) as pokpur,
                    pul.icd1,
                    pul.ket1
                    FROM billing_binap as bi
                    LEFT JOIN master_pokgolpas as pok ON pok.kode = bi.golpaskd
                    LEFT JOIN master_pasien as pas ON pas.kode = bi.norekmed
                    LEFT JOIN master_satpur as sat ON sat.kode = pas.satuankd AND pok.kelompok='MIL AD'
                    LEFT JOIN pulang_inap as pul ON pul.noreg = bi.noreg
                    WHERE pul.icd1 <> '' AND bi.sr_deleted <> 'T' AND bi.tgllapor BETWEEN '".$from."' AND '".$to."'
                ) as tes"))
                ->select(DB::raw("icd1,
                ket1,
                COUNT(pokpur='SATPUR' OR NULL) AS satpur,
                COUNT(pokpur='SAT BANPUR' OR NULL) AS banpur,
                COUNT(pokpur='SAT BANMIN' OR NULL) AS banmin,
                COUNT(pokpur='PNS AD' OR NULL) AS pns_ad,
                COUNT(pokpur='KEL AD' OR NULL) AS kel_ad,
                COUNT(pokpur='MIL LAIN' OR NULL) AS mil_lain,
                COUNT(pokpur='PNS LAIN' OR NULL) AS pns_lain,
                COUNT(pokpur='KEL LAIN' OR NULL) AS kel_lain,
                COUNT(pokpur='BPJS' OR NULL) AS bpjs,
                COUNT(pokpur='UMUM' OR NULL) AS umum,
                COUNT(pokpur='DIKMA' OR NULL) AS dikma,
                COUNT(pokpur='TARUNA' OR NULL) AS taruna,
                COUNT(pokpur='PENSIUN' OR NULL) AS pensiun,
                COUNT(icd1) as jumlah"))
                ->groupBy('icd1','ket1')->orderBy('jumlah','desc')->limit(10)->get();

        $icdJalan = DB::connection('simrs')->table(DB::raw("(SELECT
                    (CASE 
                        WHEN pok.kelompok='MIL AD' AND sat.satpur IS NOT NULL THEN sat.satpur 
                        WHEN pok.kelompok='MIL AD' AND sat.satpur IS NULL THEN 'SAT BANMIN' 
                        ELSE pok.kelompok 
                    END) as pokpur,
                    pul.icd1,
                    pul.ket1
                    FROM billing_bjalan as bi
                    LEFT JOIN master_pokgolpas as pok ON pok.kode = bi.golpaskd
                    LEFT JOIN master_pasien as pas ON pas.kode = bi.norekmed
                    LEFT JOIN master_satpur as sat ON sat.kode = pas.satuankd AND pok.kelompok='MIL AD'
                    LEFT JOIN pulang_jalan as pul ON pul.noreg = bi.noreg
                    WHERE pul.icd1 <> '' AND bi.sr_deleted <> 'T' AND bi.tgllapor BETWEEN '".$from."' AND '".$to."'
                ) as tes"))
                ->select(DB::raw("icd1,
                ket1,
                COUNT(pokpur='SATPUR' OR NULL) AS satpur,
                COUNT(pokpur='SAT BANPUR' OR NULL) AS banpur,
                COUNT(pokpur='SAT BANMIN' OR NULL) AS banmin,
                COUNT(pokpur='PNS AD' OR NULL) AS pns_ad,
                COUNT(pokpur='KEL AD' OR NULL) AS kel_ad,
                COUNT(pokpur='MIL LAIN' OR NULL) AS mil_lain,
                COUNT(pokpur='PNS LAIN' OR NULL) AS pns_lain,
                COUNT(pokpur='KEL LAIN' OR NULL) AS kel_lain,
                COUNT(pokpur='BPJS' OR NULL) AS bpjs,
                COUNT(pokpur='UMUM' OR NULL) AS umum,
                COUNT(pokpur='DIKMA' OR NULL) AS dikma,
                COUNT(pokpur='TARUNA' OR NULL) AS taruna,
                COUNT(pokpur='PENSIUN' OR NULL) AS pensiun,
                COUNT(icd1) as jumlah"))
                ->groupBy('icd1','ket1')->orderBy('jumlah','desc')->limit(10)->get();

        // $icdInap = DB::connection('simrs')->table('pulang_inap')->select(DB::raw('icd1,ket1,COUNT(icd1) as jml'))
        //             ->where('sr_deleted', '!=' , 'T')->where('icd1', '!=' , '')->whereBetween('tgl', [$from, $to])
        //             ->groupBy('icd1','ket1')->orderBy('jml','desc')->limit(10)->get();

        // $icdJalan = DB::connection('simrs')->table('pulang_jalan')->select(DB::raw('icd1,ket1,COUNT(icd1) as jml'))
        //             ->where('sr_deleted', '!=' , 'T')->where('icd1', '!=' , '')->whereBetween('tgl', [$from, $to])
        //             ->groupBy('icd1','ket1')->orderBy('jml','desc')->limit(10)->get();                    

        // $unionInap = DB::connection('simrs')->table('pulang_inap')->select(DB::raw('icd1,ket1,COUNT(icd1) as jml'))
        //             ->where('sr_deleted', '!=' , 'T')->where('icd1', '!=' , '')->whereBetween('tgl', [$from, $to])
        //             ->groupBy('icd1','ket1');

        // $icdUnion =  DB::connection('simrs')->table('pulang_jalan')->select(DB::raw('icd1,ket1,COUNT(icd1) as jml'))
        //             ->where('sr_deleted', '!=' , 'T')->where('icd1', '!=' , '')->whereBetween('tgl', [$from, $to])
        //             ->groupBy('icd1','ket1')
        //             ->unionAll($unionInap)
        //             ->orderBy('jml','desc')->limit(10)->get();

        return view('laporan.10penyakit',compact('icdInap','icdJalan'));
    }
}
