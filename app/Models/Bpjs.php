<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bpjs extends Model
{
    public static function bridgingBpjs($metode, $urlApi)
    {
        $data = "20337";
        $secretKey = "4tW3926623";
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);

        $headers = [
            'X-cons-id: '.$data,
            'X-timestamp: '.$tStamp,
            'X-signature: '.$encodedSignature,
            'ContentType: application/json',
            'Accept : application/json'
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://new-api.bpjs-kesehatan.go.id:8080".$urlApi); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        return $server_output;
    }

    public static function cariSep($sep){
        $hasil = self::bridgingBpjs("GET","/new-vclaim-rest/SEP/".$sep);
        return $hasil;
    }
}
