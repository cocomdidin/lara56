<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    
});

Auth::routes();

Route::get('/', 'DashboardController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');
    Route::resource('pasiens','PasienController');
    Route::get('/laporan', 'LaporanController@index');
    Route::get('/laporan/10penyakit/{from}/{to}', 'LaporanController@penyakitterbesar');
});
