<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>Hash::make('123456')
        ]);
        $admin->assignRole('Admin','User');

        $user = User::create([
            'name'=>'User',
            'email'=>'user@user.com',
            'password'=>Hash::make('123456')
        ]);
        $user->assignRole('User');
    }
}
