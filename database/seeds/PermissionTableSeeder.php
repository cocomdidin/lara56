<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Gunakan '_' untuk nama permission yang lebih dari satu kata. contoh 'product_baru-create'
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete'
         ];


         foreach ($permissions as $permission) {
             $group = explode('-',$permission)[0];
              Permission::create(['name' => $permission, 'group' => $group]);
         }
    }
}
