<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $Menus = [
            ['id'=>1, 'parent_id'=>0, 'role_id'=>1, 'title'=>'Administrator', 'url'=>'#', 'icon'=>'how_to_reg', 'icon_mini'=>'', 'menu_order'=>99, 'description'=>''],
            ['id'=>2, 'parent_id'=>1, 'role_id'=>1, 'title'=>'Users Management', 'url'=>'/users', 'icon'=>'', 'icon_mini'=>'UM', 'menu_order'=>1, 'description'=>''],
            ['id'=>3, 'parent_id'=>1, 'role_id'=>1, 'title'=>'Roles Management', 'url'=>'/roles', 'icon'=>'', 'icon_mini'=>'RM', 'menu_order'=>2, 'description'=>''],
            ['id'=>4, 'parent_id'=>0, 'role_id'=>2, 'title'=>'Products', 'url'=>'/products', 'icon'=>'ballot', 'icon_mini'=>'P', 'menu_order'=>1, 'description'=>''],
         ];

         foreach ($Menus as $Menu) {
              Menu::insert($Menu);
         }
    }
}
