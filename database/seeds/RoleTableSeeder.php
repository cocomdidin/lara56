<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'Admin',
            'guard_name'=>'web'
        ]);
        $role->givePermissionTo(Permission::pluck('name'));

        $role = Role::create([
            'name' => 'User',
            'guard_name'=>'web'
        ]);
        $role->givePermissionTo(Permission::where('group','product')->pluck('name'));
    }
}
