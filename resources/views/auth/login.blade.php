<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>
     Login
    </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
<!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <!-- <link rel="stylesheet" href="{{asset('assets/css/roboto/Roboto-icons.css')}}"> -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
<!-- CSS Files -->

<link href="{{asset('assets/css/material-dashboard.min40a0.css?v=2.0.2')}}" rel="stylesheet" />

<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />


</head>

    <body class="off-canvas-sidebar">


        <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white" id="navigation-example">
	<div class="container">
    <div class="navbar-wrapper">



			<a class="navbar-brand" >Login Page</a>
		</div>

		<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
      <span class="sr-only">Toggle navigation</span>
			<span class="navbar-toggler-icon icon-bar"></span>
			<span class="navbar-toggler-icon icon-bar"></span>
			<span class="navbar-toggler-icon icon-bar"></span>
		</button>

	    <div class="collapse navbar-collapse justify-content-end">


            <ul class="navbar-nav">
    <li class="nav-item">
        <a href="/" class="nav-link">
            <i class="material-icons">dashboard</i>
            Dashboard
        </a>
    </li>
    <li class= "nav-item  active ">
        <a href="/login" class="nav-link">
            <i class="material-icons">fingerprint</i>
            Login
        </a>
    </li>
</ul>





	    </div>
	</div>
</nav>
<!-- End Navbar -->


        <div class="wrapper wrapper-full-page">



<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../../assets/img/login.jpg'); background-size: cover; background-position: top center;">
  <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
  <div class="container">
    <div class="col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto">
      <!-- <form class="form" method="" action="#"> -->
      <form id="form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
      @csrf
        <div class="card card-login card-hidden">
          <div class="card-header card-header-rose text-center">
            <h4 class="card-title">Login</h4>
          </div>
          <div class="card-body ">

            <!-- <span class="bmd-form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" class="form-control" placeholder="First Name...">
              </div>
            </span> -->
            <span class="bmd-form-group  is-invalid">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input name="email" type="email" class="form-control" placeholder="Email..." value="{{ old('email') }}" required autofocus>
              </div>
            </span>
            <span class="bmd-form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input name="password" type="password" class="form-control " placeholder="Password..." required>
              </div>
            </span>
          </div>
          <div class="card-footer justify-content-center">
            <a onclick="$('#form').submit()" class="btn btn-rose btn-link btn-lg">MASUK</a>
          </div>
          @if ($errors->has('password'))
          <div class="alert alert-danger" align="center">
            <span> User dan password tidak cocok!</span>
          </div>
          @endif
        </div>
      </form>
    </div>
  </div>
  <footer class="footer" >
    <div class="container">
        <nav class="float-left">
          <ul>
              <li>
                  <a>
                    {{ config('app.name', 'Laravel') }}
                  </a>
              </li>
          </ul>
        </nav>
        <div class="copyright float-right">
            &copy; {{ date('Y') }}, Created by <a target="_blank"><b>Tim IT</b> {{ config('app.name', 'Laravel') }}</a>
        </div>
    </div>
</footer>

</div>


        </div>



<!--   Core JS Files   -->
<script src="{{asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}" ></script>



<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('assets/js/material-dashboard.min40a0.js?v=2.0.2')}}" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/demo/demo.js')}}"></script>


  <script>
  $(document).ready(function(){
    demo.checkFullPageBackgroundImage();setTimeout(function(){
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
      }, 700);});

</script>

    </body>
</html>
