@extends('layout.master')
@section('navbrand')
    <a class="navbar-brand" href="/home">Dashboard</a>
@endsection
@section('btnSreach')
    <form class="navbar-form">
        <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </div>
    </form>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">weekend</i>
                </div>
                <p class="card-category">Bookings</p>
                <h3 class="card-title">184</h3>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-danger">warning</i>
                    <a href="#pablo">Get More Space...</a>
                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">equalizer</i>
                </div>
                <p class="card-category">Website Visits</p>
                <h3 class="card-title">75.521</h3>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">local_offer</i> Tracked from Google Analytics
                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">store</i>
                </div>
                <p class="card-category">Revenue</p>
                <h3 class="card-title">$34,245</h3>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="fa fa-twitter"></i>
                </div>
                <p class="card-category">Followers</p>
                <h3 class="card-title">+245</h3>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                </div>
                </div>
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-rose" data-header-animation="true">
                <div class="ct-chart" id="websiteViewsChart"></div>
                </div>
                <div class="card-body">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Website Views</h4>
                <p class="card-category">Last Campaign Performance</p>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-success" data-header-animation="true">
                <div class="ct-chart" id="dailySalesChart"></div>
                </div>
                <div class="card-body">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Daily Sales</h4>
                <p class="card-category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-info" data-header-animation="true">
                <div class="ct-chart" id="completedTasksChart"></div>
                </div>
                <div class="card-body">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Completed Tasks</h4>
                <p class="card-category">Last Campaign Performance</p>
                </div>
                <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title">Simple Table</h4>
                    </div>
                    <div class="card-body">
                    <div class="table-responsive"> 
                        <table class="table">
                        <thead>
                            <tr>
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>Job Position</th>
                            <th>Since</th>
                            <th class="text-right">Salary</th>
                            <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td class="text-center">1</td>
                            <td>Andrew Mike</td>
                            <td>Develop</td>
                            <td>2013</td>
                            <td class="text-right">&euro; 99,225</td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info">
                                <i class="material-icons">person</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-success">
                                <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-danger">
                                <i class="material-icons">close</i>
                                </button>
                            </td>
                            </tr>
                            <tr>
                            <td class="text-center">2</td>
                            <td>John Doe</td>
                            <td>Design</td>
                            <td>2012</td>
                            <td class="text-right">&euro; 89,241</td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info btn-round">
                                <i class="material-icons">person</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-success btn-round">
                                <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-danger btn-round">
                                <i class="material-icons">close</i>
                                </button>
                            </td>
                            </tr>
                            <tr>
                            <td class="text-center">3</td>
                            <td>Alex Mike</td>
                            <td>Design</td>
                            <td>2010</td>
                            <td class="text-right">&euro; 92,144</td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info btn-link">
                                <i class="material-icons">person</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-success btn-link">
                                <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-danger btn-link">
                                <i class="material-icons">close</i>
                                </button>
                            </td>
                            </tr>
                            <tr>
                            <td class="text-center">4</td>
                            <td>Mike Monday</td>
                            <td>Marketing</td>
                            <td>2013</td>
                            <td class="text-right">&euro; 49,990</td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info btn-round">
                                <i class="material-icons">person</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-success btn-round">
                                <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-danger btn-round">
                                <i class="material-icons">close</i>
                                </button>
                            </td>
                            </tr>
                            <tr>
                            <td class="text-center">5</td>
                            <td>Paul Dickens</td>
                            <td>Communication</td>
                            <td>2015</td>
                            <td class="text-right">&euro; 69,201</td>
                            <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info">
                                <i class="material-icons">person</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-success">
                                <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" class="btn btn-danger">
                                <i class="material-icons">close</i>
                                </button>
                            </td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <!-- kalo nak nambah tabel lain pake 'col-md-12' -->
        </div>

    </div>
@endsection

@section('script')
<script>
  $(document).ready(function(){
    $().ready(function(){
      
          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
              window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
              clearInterval(simulateWindowResize);
          }, 1000);

    });
  });
</script>


  
<!-- Sharrre libray -->
<!-- <script src="../assets/demo/jquery.sharrre.js"></script> -->


  <script>
    $(document).ready(function(){
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
      
      
    });
</script>
@endsection