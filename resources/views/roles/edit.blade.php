@extends('layout.master')
@section('navbrand')
    <a class="navbar-brand" href="#">Edit Role</a>
@endsection
@section('btnSreach')
    <form class="navbar-form">
        <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </div>
    </form>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="material-icons">close</i>
            </button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
        <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
                <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">Edit Role
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
                </div>
            </h4>
        </div>
        <div class="card-body">
            {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Permission:</strong>
                        <br/>
                            {!! Form::macro('ckbox',function($name,$value,$chek){
                                return '<input class="form-check-input" type="checkbox" value='.$value.' name='.$name.' '.$chek.'> ';
                            }); !!}
                            <div class="table-responsive">
                                <table class="table">
                                <thead>
                                    <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th>List</th>
                                    <th>Create</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=0; @endphp
                                    @foreach($permissionGroup as $gp)
                                    <tr>
                                        <td  class="text-center">{{ ++$i }}</td>
                                        <td  class="text-left">{{$gp->group}}</td>
                                        <!-- list permission -->
                                        <td>
                                            @foreach($permission as $value)
                                            @if($gp->group==$value->group && strpos($value->name, '-list') !== false)
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {!! Form::ckbox('permission[]',$value->id,in_array($value->id, $rolePermissions) ? "checked" : "") !!}
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @endforeach
                                        </td>
                                        <!-- create permission -->
                                        <td>
                                            @foreach($permission as $value)
                                            @if($gp->group==$value->group && strpos($value->name, '-create') !== false)
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {!! Form::ckbox('permission[]',$value->id,in_array($value->id, $rolePermissions) ? "checked" : "") !!}
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @endforeach
                                        </td>
                                        <!-- edit permission -->
                                        <td>
                                            @foreach($permission as $value)
                                            @if($gp->group==$value->group && strpos($value->name, '-edit') !== false)
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {!! Form::ckbox('permission[]',$value->id,in_array($value->id, $rolePermissions) ? "checked" : "") !!}
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @endforeach
                                        </td>
                                        <!-- delete permission -->
                                        <td>
                                            @foreach($permission as $value)
                                            @if($gp->group==$value->group && strpos($value->name, '-delete') !== false)
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    {!! Form::ckbox('permission[]',$value->id,in_array($value->id, $rolePermissions) ? "checked" : "") !!}
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>

                        <!-- @foreach($permission as $value)
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::ckbox('permission[]',$value->id,in_array($value->id, $rolePermissions) ? "checked" : "") !!}
                                    {{ $value->name }}
                                    <span class="form-check-sign">
                                    <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        @endforeach -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection