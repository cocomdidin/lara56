@extends('layout.master')
@section('navbrand')
    <a class="navbar-brand" href="#">Role Management</a>
@endsection
@section('btnSreach')
    <form class="navbar-form">
        <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </div>
    </form>
@endsection
@section('content')
        <div class="row">
            <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                    </button>
                    <span>{{ $message }}</span>
                </div>
            @endif
                <div class="card">
                    <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title">Daftar Roles
                        <div class="pull-right">
                        @can('role-create')
                            <a class="btn btn-success" href="{{ route('roles.create') }}"> Buat Role</a>
                            @endcan
                        </div>
                    </h4>
                    </div>
                    <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                        <thead>
                            <tr>
                            <th class="text-center">#</th>
                            <th>Nama Role</th>
                            <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $key => $role)
                            <tr>
                                <td  class="text-center">{{ ++$i }}</td>
                                <td>{{ $role->name }}</td>
                                <td class="td-actions text-right">
                                    <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">
                                        <i class="material-icons">visibility</i>
                                    </a>
                                    @can('role-edit')
                                        <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">
                                            <i class="material-icons">edit</i>
                                        </a>
                                    @endcan
                                    @can('role-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                            {!! Form::button('<i class="material-icons">close</i>',['type'=>'submit','class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <!-- kalo nak nambah tabel lain pake 'col-md-12' -->
        </div>

{!! $roles->render() !!}


@endsection