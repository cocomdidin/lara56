@extends('layout.master')
@section('navbrand')
    <a class="navbar-brand" href="#">10 Besar Penyakit</a>
@endsection
@section('btnSreach')
    <!-- <form class="navbar-form">
        <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </div>
    </form> -->
@endsection
@section('content')
    <div class="row">   
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">Rawat jalan</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                    <thead >
                        <tr class="table-light">
                            <th class="text-center">#</th>
                            <th>ICD 10</th>
                            <th>DIAGNOSA</th>
                            <th>SATPUR</th>
                            <th>BANPUR</th>
                            <th>BANMIN</th>
                            <th>PNS AD</th>
                            <th>KEL AD</th>
                            <th>MIL LAIN</th>
                            <th>PNS LAIN</th>
                            <th>KEL LAIN</th>
                            <th>BPJS</th>
                            <th>UMUM</th>
                            <th>DIKMA</th>
                            <th>TARUNA</th>
                            <th>PENSIUN</th>
                            <th>JUMLAH</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach ($icdJalan as $key => $icdJ)
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-upercase">{{ $icdJ->icd1 }}</td>
                            <td class="text-lowercase">{{ $icdJ->ket1 }}</td>
                            <td class="text-center">{{ $icdJ->satpur }}</td>
                            <td class="text-center">{{ $icdJ->banpur }}</td>
                            <td class="text-center">{{ $icdJ->banmin }}</td>
                            <td class="text-center">{{ $icdJ->pns_ad }}</td>
                            <td class="text-center">{{ $icdJ->kel_ad }}</td>
                            <td class="text-center">{{ $icdJ->mil_lain }}</td>
                            <td class="text-center">{{ $icdJ->pns_lain }}</td>
                            <td class="text-center">{{ $icdJ->kel_lain }}</td>
                            <td class="text-center">{{ $icdJ->bpjs }}</td>
                            <td class="text-center">{{ $icdJ->umum }}</td>
                            <td class="text-center">{{ $icdJ->dikma }}</td>
                            <td class="text-center">{{ $icdJ->taruna }}</td>
                            <td class="text-center">{{ $icdJ->pensiun }}</td>
                            <td class="text-center">{{ $icdJ->jumlah }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">Rawat inap</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                    <thead >
                        <tr class="table-light">
                            <th class="text-center">#</th>
                            <th>ICD 10</th>
                            <th>DIAGNOSA</th>
                            <th>SATPUR</th>
                            <th>BANPUR</th>
                            <th>BANMIN</th>
                            <th>PNS AD</th>
                            <th>KEL AD</th>
                            <th>MIL LAIN</th>
                            <th>PNS LAIN</th>
                            <th>KEL LAIN</th>
                            <th>BPJS</th>
                            <th>UMUM</th>
                            <th>DIKMA</th>
                            <th>TARUNA</th>
                            <th>PENSIUN</th>
                            <th>JUMLAH</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach ($icdInap as $key => $icdI)
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-upercase">{{ $icdI->icd1 }}</td>
                            <td class="text-lowercase">{{ $icdI->ket1 }}</td>
                            <td class="text-center">{{ $icdI->satpur }}</td>
                            <td class="text-center">{{ $icdI->banpur }}</td>
                            <td class="text-center">{{ $icdI->banmin }}</td>
                            <td class="text-center">{{ $icdI->pns_ad }}</td>
                            <td class="text-center">{{ $icdI->kel_ad }}</td>
                            <td class="text-center">{{ $icdI->mil_lain }}</td>
                            <td class="text-center">{{ $icdI->pns_lain }}</td>
                            <td class="text-center">{{ $icdI->kel_lain }}</td>
                            <td class="text-center">{{ $icdI->bpjs }}</td>
                            <td class="text-center">{{ $icdI->umum }}</td>
                            <td class="text-center">{{ $icdI->dikma }}</td>
                            <td class="text-center">{{ $icdI->taruna }}</td>
                            <td class="text-center">{{ $icdI->pensiun }}</td>
                            <td class="text-center">{{ $icdI->jumlah }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection