@extends('layout.master')
@section('link')
<style>
    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
        width:100%;
    }
</style>
@endsection
@section('navbrand')
    <a class="navbar-brand" href="#">laporan index</a>
@endsection
@section('btnSreach')
    <form class="navbar-form">
        <!-- <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </div> -->
    </form>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                <div class="card-icon">
                    <i class="material-icons">filter</i>
                </div>
                    <h4 class="card-title">Filter</h4>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control datepicker" id="datepicker1" data-date-format="DD/MM/YYYY" value='{{ date("d/m/Y") }}'>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            s/d
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control datepicker" id="datepicker2" data-date-format="DD/MM/YYYY"  value='{{ date("d/m/Y") }}'>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <select class="selectpicker" data-style="select-with-transition" title="Pilih">
                                    <option selected value="1">Semua Poli</option>
                                    @foreach ($poli as $key => $row)
                                    <option value="{{$row->kode}}">{{$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Menu Laporan</h4>
                </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button id="btn10Penyakit" class="btn btn-primary btn-link">1. Laporan 10 Besar Penyakit</button>
                        </div>
                        <div class="col-md-12">
                            <button id="btnMenahun" class="btn btn-primary btn-link">2. Laporan Penyakit Menahun</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        
        $("#datepicker1").on("dp.change", function (e) {
            $('#datepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datepicker2").on("dp.change", function (e) {
            $('#datepicker1').data("DateTimePicker").maxDate(e.date);
        });

        function fromID(tgl){
            var start = tgl;
            var xs = start.split("/");
            var ystart = xs[2]+'-'+xs[1]+'-'+xs[0];
            return ystart;
        }
        
        $("#btn10Penyakit").click( function () {
            var dari = document.getElementById('datepicker1').value;
            var sampai = document.getElementById('datepicker2').value;
            location.href = "/laporan/10penyakit/" + fromID(dari) + "/" + fromID(sampai);
        });

    });
</script>
@endsection